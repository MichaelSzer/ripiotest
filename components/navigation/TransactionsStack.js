import { createStackNavigator } from 'react-navigation';
import TransactionsHistoryView from '../views/transactionsHistory/TransactionsHistoryView';
import TransactionDetailsView from '../views/transactionDetails/TransactionDetailsView';

const TransactionsStack = createStackNavigator(
    {    
        TransactionsHistory: {
            screen: TransactionsHistoryView
        },

        TransactionDetails: {
            screen: TransactionDetailsView
        }
    },
    {
        initialRouteName: 'TransactionsHistory'
    }
);

TransactionsStack.navigationOptions = {
    title: 'TRANSACCIONES'
};

export default TransactionsStack;