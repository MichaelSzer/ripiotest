import { StyleSheet } from 'react-native';

const balanceComponentStyle = StyleSheet.create({
    mainView: {
      height: 220,
      alignItems: 'center',
      backgroundColor: '#eee',
      justifyContent: 'center'
    },
  
    balanceText: {
      marginBottom: 20,
      fontSize: 22
    },
  
    valueBitcoinText: {
      marginTop: 7,
      fontSize: 24
    },
  
    valueConverterText: {
      marginTop: 6,
      fontSize: 18
    }
  
  });

  export default balanceComponentStyle;