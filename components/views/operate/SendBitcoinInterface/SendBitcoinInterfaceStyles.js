import { StyleSheet } from 'react-native';

const sendBitcoinsInterfaceStyles = StyleSheet.create({

    mainView: {
      height: 320,
      backgroundColor: '#eee',
      justifyContent: 'center',
      alignItems: 'center'
    },
  
    sendBitcoinsText: {
      fontSize: 17,
      marginBottom: 18
    },

    transactionFeeText: {
      fontSize: 15,
      marginBottom: 6
    },

    totalCostText: {
      fontSize: 15,
      marginBottom: 18
    },
  
    directionInputView: {
      flexDirection: "row",
      justifyContent: "center",
      alignItems: 'center',
      alignSelf: "stretch",
      marginTop: 15,
      marginBottom: 5,
      height: 50
    },

    quantityInputView: {
      flexDirection: "row",
      justifyContent: "center",
      alignItems: 'center',
      alignSelf: "stretch",
      marginTop: 5,
      marginBottom: 16,
      height: 50
    },
  
    textInput: {
      width: 200,
      backgroundColor: '#fff',
      borderColor: '#000',
      borderStyle: 'solid',
      borderRadius: 10,
      marginRight: 15,
      alignItems: 'flex-end'
    },

    leftTextInInput: {
      fontSize: 14,
      width: 100
    }
  
});

export default sendBitcoinsInterfaceStyles;