import { createBottomTabNavigator } from 'react-navigation';
import TransactionsStack from './TransactionsStack';
import OperationsTopTabNavigator from './OperationsTopTabNavigator';

const rootBottomTabNavigator = createBottomTabNavigator(
    {
        OperationsScreen: {
            screen: OperationsTopTabNavigator
        },

        TransactionsScreen: {
            screen: TransactionsStack
        }
    },
    {
        initialRouteName: 'OperationsScreen',
        
        tabBarOptions: {
            labelStyle: {
                fontSize: 18,
                alignSelf: 'center',
                marginBottom: 11
            }
        }
    }
);

export default rootBottomTabNavigator;
