import React, { PureComponent } from 'react';
import { View, Text,  } from 'react-native';
import { connect } from 'react-redux';
import { getAccountBalance, getConversionRates } from '../../../../Redux/Selectors';
import { updateConversionRate } from '../../../../Redux/ActionCreators';
import AccountBalanceInterfaceStyle from './AccountBalanceInterfaceStyle';
import { asyncUpdateConversionRate } from './AccountBalanceInterfaceFunctions';

// Container Component
// b2ars is the bitcoin rate to argentine pesos 
class AccountBalanceInterface extends PureComponent {
    state = {
        conversionRateFetched: false
    }

    constructor(props){
        super(props);

        this.asyncUpdateConversionRate = asyncUpdateConversionRate.bind(this);
    }


    render(){

        const btc2ars = this.props.conversionRates["BTC"]["ARS"];

        // Convert bitcoins to pesos
        const pesos = (this.props.accountBalance * btc2ars).toFixed(2);

        return (
            <View style={AccountBalanceInterfaceStyle.mainView}>
                <Text style={AccountBalanceInterfaceStyle.balanceText}>{"Balance Actual"}</Text>
                <Text style={AccountBalanceInterfaceStyle.valueBitcoinText}>{this.props.accountBalance.toFixed(8).toString() + " BTC / Bitcoins"}</Text>
                <Text style={AccountBalanceInterfaceStyle.valueConverterText}>{"serían " + pesos.toString() + "$ ARS / Pesos"}</Text>
            </View>
        );
    }

    componentDidMount() {

        // Fetch conversion rates from Rest API
        if(!this.state.conversionRateFetched)
        {
            this.asyncUpdateConversionRate();
        }
    }
}

export default connect( (state) => ({
  accountBalance: getAccountBalance(state),
  conversionRates: getConversionRates(state)
}),
{
    updateConversionRate
})
(AccountBalanceInterface);