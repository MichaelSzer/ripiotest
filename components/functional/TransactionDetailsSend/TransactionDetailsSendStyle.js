import { StyleSheet } from 'react-native';

const transactionDetailsSendStyle = StyleSheet.create({
    mainView: {
        flex: 1,
        marginTop: 15
    },

    dateText: {
        marginTop: 50,
        fontSize: 16,
        marginRight: 8,
        alignSelf: 'flex-end'
    },

    detailsView: {
        height: 465,
        backgroundColor: '#eee'
    },

    detailsText: {
        fontSize: 22,
        marginLeft: 8
    }
});

export default transactionDetailsSendStyle;