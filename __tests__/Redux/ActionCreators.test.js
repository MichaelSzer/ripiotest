import * as actionCreators from '../../Redux/ActionCreators';
import * as actionTypes from '../../Redux/ActionTypes';

describe('Test depositBitcoins', () => {
    
    it('Test 1', () => {

        const bitcoins = 5;
        const date = "08/15";

        const expectedAction = {

            type: actionTypes.DEPOSIT_BITCOINS,
            payload: {
                amount: bitcoins,
                date
            }
        }

        expect(actionCreators.depositBitcoins(bitcoins, date)).toEqual(expectedAction);
    });

    it('Test 2', () => {

        const bitcoins = 0.2;
        const date = "10/7";

        const expectedAction = {

            type: actionTypes.DEPOSIT_BITCOINS,
            payload: {
                amount: bitcoins,
                date
            }
        }

        expect(actionCreators.depositBitcoins(bitcoins, date)).toEqual(expectedAction);
    });    
});

describe('Test sendBitcoins', () => {
    
    it('Test 1', () => {

        const direction = "1cbasdnASDASDioj12";
        const amount = 0.01;
        const fuel = 180;
        const successful = true;
        const date = "03/25";

        const expectedAction = {

            type: actionTypes.SEND_BITCOINS,
            payload: {
                direction,
                amount,
                fuel,
                successful,
                date
            }
        }

        expect(actionCreators.sendBitcoins(direction, amount, fuel, successful, date )).toEqual(expectedAction);
    });

    it('Test 2', () => {

        const direction = "1ASDSD12312xasdDASDioj12";
        const amount = 0.67;
        const fuel = 0.0000320;
        const successful = false;
        const date = "01/20";

        const expectedAction = {

            type: actionTypes.SEND_BITCOINS,
            payload: {
                direction,
                amount,
                fuel,
                successful,
                date
            }
        }

        expect(actionCreators.sendBitcoins(direction, amount, fuel, successful, date )).toEqual(expectedAction);
    });    
});

describe('Test updateConversionRate', () => {
    
    it('Test 1', () => {

        const fromCurrency = "BTC";
        const toCurrency = "ARS";
        const conversionRate = 320000;

        const expectedAction = {

            type: actionTypes.UPDATE_CONVERSION_RATE,
            payload: {
                fromCurrency,
                toCurrency,
                conversionRate
            }
        }

        expect(actionCreators.updateConversionRate(fromCurrency, toCurrency, conversionRate)).toEqual(expectedAction);
    });

    it('Test 2', () => {

        const fromCurrency = "ARS";
        const toCurrency = "USD";
        const conversionRate = 44;

        const expectedAction = {

            type: actionTypes.UPDATE_CONVERSION_RATE,
            payload: {
                fromCurrency,
                toCurrency,
                conversionRate
            }
        }

        expect(actionCreators.updateConversionRate(fromCurrency, toCurrency, conversionRate)).toEqual(expectedAction);
    });    
});

describe('Test updateTransactionFee', () => {

    it('Test 1', () => {
        
        const fee = 200;

        const expectedAction = {
            type: actionTypes.UPDATE_TRANSACTION_FEE,
            payload: {
                fee
            }
        }

        expect(actionCreators.updateTransactionFee(fee)).toEqual(expectedAction);
    });

    it('Test 2', () => {
        
        const fee = 0.008;

        const expectedAction = {
            type: actionTypes.UPDATE_TRANSACTION_FEE,
            payload: {
                fee
            }
        }

        expect(actionCreators.updateTransactionFee(fee)).toEqual(expectedAction);
    });
});
