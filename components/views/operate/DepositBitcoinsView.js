import React, {Component} from 'react';
import { StyleSheet, View, Button, TextInput, Text } from 'react-native';
import AccountBalanceInterface from './AccountBalanceInterface/AccountBalanceInterfaceComponent';
import DepositBitcoinsInterface from './DepositBitcoinInterface/DepositBitcoinsInterfaceComponent';
import SendBitcoinsInterface from './SendBitcoinInterface/SendBitcoinsInterfaceComponent';

class DepositBitcoinsView extends Component {
  static navigationOptions = {
      title: 'DEPOSITAR'
  };


  render() {

    return (
      <View style={styles.container}>
        <AccountBalanceInterface />
        <DepositBitcoinsInterface />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'stretch',
    backgroundColor: '#F5FCFF',
  }
});

export default DepositBitcoinsView;