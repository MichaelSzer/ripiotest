import { StyleSheet } from 'react-native';

const transactionItemStyle = StyleSheet.create({

    mainView: {
        flex: 1,
        flexDirection: 'row'
    },

    touchableOpacity: {
        flex: 1
    },

    leftView: {
        flex: 3,
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'flex-start'
    },

    rightView: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-end'
    },

    breakLine: {
        alignSelf: 'stretch',
        backgroundColor: '#333',
        height: 1
    },

    statusText: {
        marginLeft: 12,
        fontSize: 14
    },

    transactionDescriptionText: {
        marginLeft: 12,
        fontSize: 16
    },

    dateText: {
        marginRight: 12,
        marginTop: 4
    }

});

export default transactionItemStyle;

