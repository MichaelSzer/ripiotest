export const getAccountBalance = (state) => {
    if(typeof(state.accountBalance) === 'undefined')
        return 0;

    return state.accountBalance;
}


export const getTransactionsHistory = (state) => {
    if(typeof(state.accountHistory) === 'undefined')
        return [];
    else if(typeof(state.accountHistory.transactions) === 'undefined')
        return [];

    return state.accountHistory.transactions;
}

export const getConversionRates = (state) => {
    if(typeof(state.ratesConversion) === 'undefined')
        return {};
    
    return state.ratesConversion;
}

export const getTransactionFee = (state) => {
    if(typeof(state.transactionFee) === 'undefined')
        return 0;
    
    return state.transactionFee;
}