import * as reducers from '../../Redux/Reducers';
import * as actionTypes from '../../Redux/ActionTypes';

describe('Test Account Balance Reducer', () => {

    it('Should return initial test', () => {
        expect(reducers.accountBalanceReducer(undefined, {})).toEqual(0);
    });

    it('Action: Deposit Coins', () => {
        expect(reducers.accountBalanceReducer(143, {
            type: actionTypes.DEPOSIT_BITCOINS,
            payload: {
                amount: 50
            }
        })).toEqual(193);
    });

    it('Action: Send Coins', () => {
        expect(reducers.accountBalanceReducer(1000, {
            type: actionTypes.SEND_BITCOINS,
            payload: {
                amount: 600,
                successful: true,
                fuel: 12
            }
        })).toEqual(388);
    });
});