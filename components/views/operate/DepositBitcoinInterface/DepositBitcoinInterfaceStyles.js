import { StyleSheet } from 'react-native';

const addBitcoinsStyles = StyleSheet.create({

    mainView: {
      height: 120,
      backgroundColor: '#eee',
      justifyContent: 'center',
      alignItems: 'center'
    },
  
    addBitcoinsText: {
      fontSize: 17,
      marginBottom: 18
    },
  
    inputView: {
      flexDirection: "row",
      justifyContent: "center",
      alignSelf: "stretch",
      marginTop: 9,
      height: 40
    },
  
    bitcoinsTextInput: {
      width: 200,
      backgroundColor: '#fff',
      borderColor: '#000',
      borderStyle: 'solid',
      borderRadius: 10,
      marginRight: 15
    }
  
});

export default addBitcoinsStyles;