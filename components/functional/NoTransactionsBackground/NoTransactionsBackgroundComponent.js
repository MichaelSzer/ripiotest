import React from 'react';
import { View, Text } from 'react-native';
import style from './NoTransactionsBackgroundStyle';

const NoTransactionsBackgroundComponent = () => {

    return(
        <View style={style.mainView}>
            <Text style={style.text}>
                {"No realizaste ninguna transacción."}
            </Text>
        </View>
    );
}

export default NoTransactionsBackgroundComponent;