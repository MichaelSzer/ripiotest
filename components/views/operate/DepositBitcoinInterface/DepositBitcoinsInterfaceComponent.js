import React, { Component } from 'react';
import { View, Text, TextInput, Button, Alert } from 'react-native';
import depositBitcoinInterfaceStyles from './DepositBitcoinInterfaceStyles';
import { connect } from 'react-redux';
import { depositBitcoins } from '../../../../Redux/ActionCreators';

// Container Component
class DepositBitcoinsInterface extends Component {

    constructor(props){
        super(props);

        this.state = {
            text: ""
        };

        this.depositBitcoins = this.depositBitcoins.bind(this);
        this.confirmDepositBitcoins = this.confirmDepositBitcoins.bind(this);
    }

    render(){
        return (
        <View style={depositBitcoinInterfaceStyles.mainView}>
            <Text style={depositBitcoinInterfaceStyles.addBitcoinsText}>{"Agregar Bitcoins"}</Text>
            <View style={depositBitcoinInterfaceStyles.inputView}>
                <TextInput
                    style={depositBitcoinInterfaceStyles.bitcoinsTextInput} 
                    editable={true}
                    maxLength={12}
                    keyboardType={"numeric"}
                    placeholder={"27.035"}  
                    onChangeText={ (text) => this.setState({text}) }
                    ref={ textInputComp => { this.textInputComp = textInputComp; }}
                />
                <Button 
                    title={"Depositar"}
                    onPress={() => this.confirmDepositBitcoins() }
                />
            </View>
        </View>
        );
    }

    confirmDepositBitcoins = () => {

        const bitcoins = Number(this.state.text);

        // Check that inputs are valid
        if(bitcoins <= 0)
        {
            Alert.alert(
                'Depósito de Bitcoins invalido',
                'Porfavor ingrese una cantidad de Bitcoins mayor a 0.'
            );
            console.log("Cantidad de Bitcoins a depositar invalida.");
            return;
        }

        Alert.alert(
            'Confirmar depósito',
            'Estás seguro que querés depositar ' + bitcoins.toFixed(8).toString() + ' Bitcoins ?',
            [
                {
                    text: 'Cancelar',
                    style: 'cancel'
                },
    
                {
                    text: 'Confirmar',
                    onPress: () => { this.depositBitcoins() }
                }
            ]
        );
    }

    depositBitcoins = () => {
        
        const bitcoins = Number(this.state.text);
        this.textInputComp.clear();

        this.setState({ text: "" });

        // Get today date
        const today = new Date();
        const day = today.getDate().toString().padStart(2, '0');
        const month = (today.getMonth() + 1).toString().padStart(2, '0'); // January is 0...
        const date = month + '/' + day;

        // Redux - Dispatch - ActionCreator
        this.props.depositBitcoins(bitcoins, date);

        Alert.alert(
            'Depósito exitoso',
            'Acabás de depositar ' + bitcoins.toString() + ' bitcoins.'
        );
        console.log( bitcoins.toString() + " fueron depositados.");
    }
  }

  export default connect( null, { depositBitcoins } )(DepositBitcoinsInterface);