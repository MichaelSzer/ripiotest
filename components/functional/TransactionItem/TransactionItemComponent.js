import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import TransactionItemDeposit from './TransactionItemDepositComponent';
import TransactionItemSend from './TransactionItemSendComponent';
import PropTypes from 'prop-types';

const TransactionItemComponent = ( { transaction, onPress } ) => {

    let renderComponent;

    if(!transaction.deposit)
    {
        // if SEND_BITCOINS
        renderComponent = <TransactionItemSend transaction={transaction} onPress={onPress} />;

    }
    else
    {
        // If DEPOSIT_BITCOINS
        renderComponent = <TransactionItemDeposit transaction={transaction} onPress={onPress} />;
    }

    return (
        <View>
            {renderComponent}
        </View>
    );
}

// Determine arguments type
TransactionItemComponent.propTypes = {
    transaction: PropTypes.shape({
        id: PropTypes.number.isRequired,
        direction: PropTypes.string,
        amount: PropTypes.number.isRequired,
        date: PropTypes.string.isRequired,
        successful: PropTypes.bool.isRequired,
        fuel: PropTypes.number
    })
}

export default TransactionItemComponent;