import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
    mainView: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center'
    },

    text: {
        fontSize: 20
    }
});

export default style;