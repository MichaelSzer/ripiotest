import React, {Component} from 'react';
import { Provider } from 'react-redux';
import store from './Redux/Store'; 
import { createAppContainer } from 'react-navigation';
import RootBottomTabNavigator from './components/navigation/RootBottomTabNavigator';

const AppContainer = createAppContainer(RootBottomTabNavigator);

export default class App extends Component {
  
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}
