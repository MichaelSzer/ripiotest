import React, { Component } from 'react';
import { View } from 'react-native';
import TransactionDetailsSendComponent from '../../functional/TransactionDetailsSend/TransactionDetailsSendComponent';
import TransactionDetailsDepositComponent from '../../functional/TransactionDetailsDeposit/TransactionDetailsDepositComponent';
import style from './TransactionDetailsStyle';

class TransactionDetailsView extends Component {
    static navigationOptions = {
        title: 'Detalles',
        headerStyle: {
            backgroundColor: '#ddd'
        }
    };

    render(){

        const transaction = this.props.navigation.getParam('transaction');

        let renderComponent;

        // Is it a deposit?
        if(transaction.deposit)
        {
            renderComponent = <TransactionDetailsDepositComponent transaction={transaction} />;
        }
        else
        {
            renderComponent = <TransactionDetailsSendComponent transaction={transaction} />;
        }

        return ( 
            <View style={style.mainView}>
                {renderComponent}
            </View>
        );
    }
}



export default TransactionDetailsView;