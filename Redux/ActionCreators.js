import { DEPOSIT_BITCOINS, SEND_BITCOINS, UPDATE_CONVERSION_RATE, UPDATE_TRANSACTION_FEE } from "./ActionTypes";

// e.g. depositBitcoins(1.24);
export const depositBitcoins = ( bitcoins, date ) => {
    
    return {
        type: DEPOSIT_BITCOINS,
        payload: {
            amount: bitcoins,
            date
        }
    }
}

// Make a transaction, send bitcoins.
// e.g. sendBitcoins(1asdqwASx...ASDxca, 0.1, 0.00012, true, 5/16, 10251);
export const sendBitcoins = ( direction, amount, fuel, successful, date ) => {
    
    return {
        type: SEND_BITCOINS,
        payload: {
            direction,
            amount,
            fuel,
            successful,
            date
        }
    }
}

// e.g. updateConversionRate("BTC", "ARS", 320215.21);
// The conversion from bitcoins to argentine pesos is of 320215.21 ( 1 BTC = 320215.21 ARS ) 
export const updateConversionRate = ( fromCurrency, toCurrency, conversionRate) => {
    
    return {
        type: UPDATE_CONVERSION_RATE,
        payload: {
            fromCurrency,
            toCurrency,
            conversionRate
        }
    }
}

// Update the transaction fee of Bitcoins
export const updateTransactionFee = ( fee ) => {

    return {
        type: UPDATE_TRANSACTION_FEE,
        payload: {
            fee
        }
    }
}

