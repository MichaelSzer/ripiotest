import { createMaterialTopTabNavigator } from 'react-navigation';
import TransactionsStack from './TransactionsStack';
import DepositBitcoinsView from '../views/operate/DepositBitcoinsView';
import SendBitcoinsView from '../views/operate/SendBitcoinsView';

const operationsTopTabNavigator = createMaterialTopTabNavigator(
    {
        DepositBitcoinsScreen: {
            screen: DepositBitcoinsView
        },

        SendBitcoinsScreen: {
            screen: SendBitcoinsView
        }
    },
    {
        initialRouteName: 'DepositBitcoinsScreen',
        
        tabBarOptions: {
            labelStyle: {
                fontSize: 18,
                alignSelf: 'center'
            },

            tabStyle: {
                // backgroundColor: '#2196F3'  <-- Default color of MaterialTopTabNavigator
            }
        }
    }
);

operationsTopTabNavigator.navigationOptions = {
    title: 'OPERACIONES'
};

export default operationsTopTabNavigator;