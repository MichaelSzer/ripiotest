import { Alert } from 'react-native';

export function confirmSendBitcoins( direction, bitcoins ) {

    // Check that inputs are valid
    if(direction == "")
    {
        console.log("Dirección invalida.");
        Alert.alert(
            'Dirección invalida',
            'Porfavor ingrese una dirección válida.'
        );
        return;
    }

    if(bitcoins <= 0)
    {
        console.log("Cantidad de Bitcoins invalida.");
        Alert.alert(
            'Envío invalido',
            'Porfavor ingrese una cantidad de Bitcoins mayor a 0.'
        );
        return;
    }

    Alert.alert(
        'Confirmar envío',
        'Estás seguro que querés enviar ' + bitcoins.toFixed(8).toString() + ' Bitcoins a la dirección ' + direction + ' ?',
        [
            {
                text: 'Cancelar',
                style: 'cancel'
            },

            {
                text: 'Confirmar',
                onPress: () => { this.processSendBitcoins( direction, bitcoins ) }
            }
        ]
    );
}

export function processSendBitcoins( direction, bitcoins ) {

    // Get Transaction fee and convert it into Bitcoins
    const feeSatoshi = this.props.transactionFee;

    const feeBitcoins = feeSatoshi * 0.00000001;

    const totalCost = bitcoins + feeBitcoins;

    // Redux call updating
    const balance = this.props.accountBalance;

    const newBalance = balance - totalCost;

    let successful = false;

    if(newBalance < 0)
    {
        // Transaction failed
        successful = false;
        console.log("La transaccion fallo, saldo insuficiente.");

        Alert.alert(
            'Transacción fallida',
            'No tenés la cantidad suficiente de Bitcoins para realizar el envío. Te faltan ' + Math.abs(newBalance).toString() + ' Bitcoins para poder realizarlo.'
        );
    }
    else
    {
        // Transaction succeed
        successful = true;
        console.log("La transaccion fue exitosa. Acabás de mandar " + bitcoins.toString() + ".");
    
        Alert.alert(
            'Transacción exitosa.',
            'Enviaste ' + bitcoins.toString() + ' Bitcoins de forma exitosa.'
        );
    }

    // Clean quantity input field
    this.setState({ quantity: ""});
    this.quantityInputComp.clear();

    // Get today date
    const today = new Date();
    const day = today.getDate().toString().padStart(2, '0');
    const month = (today.getMonth() + 1).toString().padStart(2, '0'); // January is 0...
    const date = month + '/' + day;

    this.props.sendBitcoins( direction, bitcoins, feeBitcoins, successful, date );
}

export async function asyncUpdateTransactionFee(){

    // In Satoshi unit
    const fee = await fetch('https://bitcoinfees.earn.com/api/v1/fees/recommended')
                .then(function(response) {
                    return response.json();
                })
                .then(function(myJson){
                    
                    const fee = myJson.fastestFee;                    
                    return fee;
                })
                .catch(error => console.log('Error: ', error));

    this.props.updateTransactionFee(fee);

    // After the fee has loaded
    this.setState({ feeLoaded: true });
}