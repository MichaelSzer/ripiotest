import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import style from './TransactionItemStyle';


const TransactionItemDeposit = ( { transaction, onPress } ) => {

    return (
        <View style={{ height: 80 }}>
            <TouchableOpacity
                style={style.touchableOpacity}
                onPress={ () => onPress(transaction) }>
                <View style={style.mainView}>
                    <View style={style.leftView}>
                        <Text style={style.transactionDescriptionText}>{"Cantidad de depósito " + transaction.amount.toFixed(8).toString() + " Bitcoins."}</Text>
                        <Text style={style.statusText}>{"Depósito " + ( transaction.successful ? "exitoso" : "fallido" ) + "."}</Text>
                    </View>
                    <View style={style.rightView}>
                        <Text style={style.dateText}>{transaction.date.toString()}</Text>
                    </View>
                </View>
                <View style={style.breakLine} />
            </TouchableOpacity>
        </View>
    );
}

export default TransactionItemDeposit;