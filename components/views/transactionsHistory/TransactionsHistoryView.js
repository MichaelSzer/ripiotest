import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { connect } from 'react-redux';
import TransactionItem from '../../functional/TransactionItem/TransactionItemComponent';
import NoTransactionsBackground from '../../functional/NoTransactionsBackground/NoTransactionsBackgroundComponent'
import { getTransactionsHistory } from '../../../Redux/Selectors';

/*const Header = () => {
    return(
        <View style={headerStyle.headerView}>
            <Text style={headerStyle.headerText}>
                {"Historial de Envíos"}
            </Text>
        </View>
    );
}

const headerStyle = StyleSheet.create({
    headerView: {
        height: 90,
        backgroundColor: '#ddd',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },

    headerText: {
        fontSize: 28,
        marginBottom: 8
    }
});*/



class TransactionsHistoryView extends Component {
    static navigationOptions = {
        title: 'Historial',
        headerStyle: {
            backgroundColor: '#ddd'
        }
    };
    
    constructor(props){
        super(props);

        this.showTransactionDetails = this.showTransactionDetails.bind(this);
    }


    showTransactionDetails(transaction){

        console.log("Showing transaction...");
        this.props.navigation.navigate('TransactionDetails', { transaction });
    }

    render(){

        let toRender;

        if(this.props.transactions.length > 0)
        {
            toRender = <FlatList
                                data={this.props.transactions}
                                renderItem={({ item }) => <TransactionItem transaction={item} onPress={this.showTransactionDetails} /> }
                                keyExtractor={( item ) => item.id.toString() } />; 
        }
        else
        {
            toRender = <NoTransactionsBackground />;
        }

        return(
            <View style={{ flex: 1 }}>
                {toRender}
            </View>
        );
    }
}

export default connect( (state) => ({
    transactions: getTransactionsHistory(state)
}))(TransactionsHistoryView);