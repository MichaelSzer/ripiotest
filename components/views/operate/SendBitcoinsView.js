import React, {Component} from 'react';
import { StyleSheet, View, Button, TextInput, Text } from 'react-native';
import AccountBalanceInterface from './AccountBalanceInterface/AccountBalanceInterfaceComponent';
import SendBitcoinsInterface from './SendBitcoinInterface/SendBitcoinsInterfaceComponent';

class SendBitcoinsView extends Component {
  static navigationOptions = {
      title: 'ENVIAR'
  };


  render() {

    return (
      <View style={styles.container}>
        <AccountBalanceInterface />
        <SendBitcoinsInterface />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'stretch',
    backgroundColor: '#F5FCFF',
  }
});

export default SendBitcoinsView;