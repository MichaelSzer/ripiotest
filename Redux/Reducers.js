import { combineReducers } from 'redux';
import { DEPOSIT_BITCOINS, SEND_BITCOINS, UPDATE_CONVERSION_RATE, UPDATE_TRANSACTION_FEE } from "./ActionTypes";


// ---------------------------------- ACCOUNT BALANCE REDUCER -----------------------------------------
export const accountBalanceReducer = ( balance = 0, action) => {

    switch(action.type){

        case DEPOSIT_BITCOINS:
            return balance + action.payload.amount;

        case SEND_BITCOINS:
            if(action.payload.successful == true)
            {
                const totalCost = action.payload.amount + action.payload.fuel; 
                return balance - totalCost;
            }
            else
                return balance;

        default:
            return balance;
    }
}

// ---------------------------------- TRANSACTION HISTORY REDUCER -----------------------------------------
export const initialTransactionHistory = {
    nextId: 10001,
    transactions: [
        /*{
            id: 10024,
            amount: 1.2,
            fuel: 0.00012,
            date: '09/05',
            successful: true,
            direction: '1basxSADASDoiu32124xmasd',
            deposit: false
        },
    
        {
            id: 10025,
            amount: 1.2,
            fuel: 0.00012,
            date: '09/05',
            successful: true,
            direction: '1basxSADASDoiu32124xmasd',
            deposit: false
        },
    
        {
            id: 10026,
            amount: 1.2,
            fuel: 0.00012,
            date: '09/05',
            successful: true,
            direction: '1basxSADASDoiu32124xmasd',
            deposit: false
        }*/
    ]
}

export const accountHistoryReducer = (transactionsHistory = initialTransactionHistory, action) => {

    switch(action.type){

        case SEND_BITCOINS:
            newTransaction = {},
            newTransaction.id = transactionsHistory.nextId,
            newTransaction.direction = action.payload.direction,
            newTransaction.amount = action.payload.amount,
            newTransaction.date = action.payload.date,
            newTransaction.successful = action.payload.successful,
            newTransaction.fuel = action.payload.fuel,
            newTransaction.deposit = false

            transactionsHistory.transactions = transactionsHistory.transactions.concat(newTransaction);
            transactionsHistory.nextId++;
            
            return transactionsHistory;

        case DEPOSIT_BITCOINS:
                newTransaction = {},
                newTransaction.id = transactionsHistory.nextId,
                newTransaction.amount = action.payload.amount,
                newTransaction.date = action.payload.date,
                newTransaction.successful = true,
                newTransaction.deposit = true
    
                transactionsHistory.transactions = transactionsHistory.transactions.concat(newTransaction);
                transactionsHistory.nextId++;
                
                return transactionsHistory;

        default:
            return transactionsHistory;
    }
}

// ---------------------------------- FEE REDUCER -----------------------------------------
// Fee is in Satoshi
export const transactionFeeReducer = ( fee = 200, action ) => {

    switch(action.type){

        case UPDATE_TRANSACTION_FEE:
            return action.payload.fee;

        default:
            return fee;
    }
}

// ---------------------------------- CONVERSION RATES REDUCER -----------------------------------------
const initialConversionRates = {
    "BTC": {
        "ARS": 350000
    }
}

export const ratesConversionReducer = ( conversationRates = initialConversionRates, action) => {

    switch(action.type){

        case UPDATE_CONVERSION_RATE:

            const fromCurrency = action.payload.fromCurrency;
            const toCurrency = action.payload.toCurrency;
            const conversionRate = action.payload.conversionRate;

            if(typeof(conversationRates[fromCurrency]) === 'undefined')
            {
                conversationRates[fromCurrency] = {
                    toCurrency: conversionRate
                };
            }
            else
            {
                conversationRates[fromCurrency][toCurrency] = conversionRate;
            }

            return conversationRates;


        default:
            return conversationRates;
    }
}


// ---------------------------------- ROOT REDUCER -----------------------------------------
export default rootReducer = combineReducers(
    {
        accountBalance: accountBalanceReducer,
        accountHistory: accountHistoryReducer,
        ratesConversion: ratesConversionReducer,
        transactionFee: transactionFeeReducer
    }
);