import React, { Component } from 'react';
import { View, Text, TextInput, Button } from 'react-native';
import sendBitcoinInterfaceStyles from './SendBitcoinInterfaceStyles';
import { confirmSendBitcoins, processSendBitcoins, asyncUpdateTransactionFee } from './SendBitcoinsFunctions';
import { connect } from 'react-redux';
import { sendBitcoins, updateTransactionFee } from '../../../../Redux/ActionCreators';
import { getAccountBalance, getTransactionFee } from '../../../../Redux/Selectors';

// Container Component
class SendBitcoinsInterface extends Component {

    constructor(props){
        super(props);

        this.state = {
            direction: "",
            quantity: "",
            feeLoaded: false
        };

        this.processSendBitcoins = processSendBitcoins.bind(this);
        this.confirmSendBitcoins = confirmSendBitcoins.bind(this);
        this.asyncUpdateTransactionFee = asyncUpdateTransactionFee.bind(this);
    }

    render(){

        // Convert Satoshi to Bitcoins
        const transactionFeeBitcoins = this.props.transactionFee * 0.00000001;

        return (
        <View style={sendBitcoinInterfaceStyles.mainView}>
            <Text style={sendBitcoinInterfaceStyles.sendBitcoinsText}>{"Enviar Bitcoins"}</Text>
            <View style={sendBitcoinInterfaceStyles.directionInputView}>
                <Text style={sendBitcoinInterfaceStyles.leftTextInInput}>
                    {"  Dirección: "}
                </Text>
                <TextInput
                    style={sendBitcoinInterfaceStyles.textInput} 
                    editable={true}
                    maxLength={35}
                    placeholder={"1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2"}  
                    onChangeText={ (direction) => this.setState({ direction }) }
                    ref={ directionInputComp => { this.directionInputComp = directionInputComp; }}
                />
            </View>
            <View style={sendBitcoinInterfaceStyles.quantityInputView}>
                <Text style={sendBitcoinInterfaceStyles.leftTextInInput}>
                    {"  Cantidad: "}
                </Text>
                <TextInput
                    style={ { ...sendBitcoinInterfaceStyles.textInput, width: 130 } } 
                    editable={true}
                    maxLength={12}
                    keyboardType={"numeric"}
                    placeholder={"6.915"}  
                    onChangeText={ (quantity) => this.setState({quantity}) }
                    ref={ quantityInputComp => { this.quantityInputComp = quantityInputComp; }}
                />
            </View>
            <Text style={sendBitcoinInterfaceStyles.transactionFeeText}>{"Costo del envío: " + transactionFeeBitcoins.toFixed(8).toString() + " BTC."}</Text>
            <Text style={sendBitcoinInterfaceStyles.totalCostText}>{"Gasto total de la transacción: " + (Number(this.state.quantity) + transactionFeeBitcoins).toFixed(8).toString() + " BTC."}</Text>
            <Button 
                title={"Enviar"}
                onPress={() => this.confirmSendBitcoins( this.state.direction, Number(this.state.quantity) ) }
            />
        </View>
        );
    }

    componentDidMount(){


        // Fetch fees
        if(!this.state.feeLoaded)
        {
            this.asyncUpdateTransactionFee();
        }
    }
  }

  export default connect( (state) => ({
    accountBalance: getAccountBalance(state),
    transactionFee: getTransactionFee(state)
  }), { sendBitcoins, updateTransactionFee } )
  (SendBitcoinsInterface);