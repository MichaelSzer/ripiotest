import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import style from './TransactionDetailsSendStyle';

// Functional / Stateless Component
const TransactionDetailsSendComponent = ( { transaction } ) => {

    const totalAmount = transaction.amount + transaction.fuel;

    return(
        <View style={style.mainView}>
            <Text style={style.dateText}>{transaction.date}</Text>
            <View style={style.detailsView}>
                <Text style={{...style.detailsText, marginTop: 40}}>
                    {"Dirección de destino: "}
                </Text>
                <Text style={{...style.detailsText, fontSize: 18}}>
                    {transaction.direction}
                </Text>
                <Text style={{...style.detailsText, marginTop: 26}}>
                    {"Monto del envío: "}
                </Text>
                <Text style={{...style.detailsText, fontSize: 18}}>
                    {transaction.amount.toFixed(8).toString() + " Bitcoins"}
                </Text>
                <Text style={{...style.detailsText, marginTop: 26}}>
                    {"Costo de la transacción: "}
                </Text>
                <Text style={{...style.detailsText, fontSize: 18}}>
                    {transaction.fuel.toFixed(8).toString() + " Bitcoins"}
                </Text>
                <Text style={{...style.detailsText, marginTop: 26}}>
                    {"Gasto total: "}
                </Text>
                <Text style={{...style.detailsText, fontSize: 18}}>
                    {totalAmount.toFixed(8).toString() + " Bitcoins"}
                </Text>
                <Text style={{...style.detailsText, marginTop: 34}}>
                    {"Estado de la transacción: " + (transaction.successful ? "Exitosa." : "Fallida.")}
                </Text>
                <Text style={{...style.detailsText, fontSize: 16, alignSelf: 'flex-end',
                                marginTop: 14, marginRight: 8 }}>
                    {"ID: " + transaction.id.toString()}
                </Text>
            </View>
        </View>
    );
}

// Determine arguments type
TransactionDetailsSendComponent.propTypes = {
    transaction: PropTypes.shape({
        id: PropTypes.number.isRequired,
        direction: PropTypes.string.isRequired,
        amount: PropTypes.number.isRequired,
        date: PropTypes.string.isRequired,
        successful: PropTypes.bool.isRequired,
        fuel: PropTypes.number.isRequired
    })
}

export default TransactionDetailsSendComponent;