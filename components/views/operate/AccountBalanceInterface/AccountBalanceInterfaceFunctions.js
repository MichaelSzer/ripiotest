export async function asyncUpdateConversionRate(){

    // Bitcoins to Argentine Pesos conversion rate
    const btc2ars = await fetch('https://ripio.com/api/v1/rates/')
                .then(function(response) {
                    return response.json();
                })
                .then(function(myJson){
                    
                    const btc2ars = myJson.rates.ARS_SELL;                    
                    return btc2ars;
                })
                .catch(error => console.log('Error: ', error));

    this.props.updateConversionRate('BTC', 'ARS', btc2ars);

    // After the fee has loaded
    this.setState({ conversionRateFetched: true });
}